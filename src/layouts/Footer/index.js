import facebook from "../../assets/img/facebook-official.png"
import twitter from "../../assets/img/twitter.png"
import "./footer.css"

function Footer() {
    return (
       <footer>
          <p>Copyright &#169; 2016. PT Company</p>
          <ul>
             <a><img className="icon-image" src={facebook} /></a>
             <a><img className="icon-image" src={twitter} /></a>
          </ul>
       </footer>
    )
}

export default Footer