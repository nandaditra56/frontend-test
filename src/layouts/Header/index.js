import "./header.css"

function Header() {
    return (
        <header>
            <nav className="container">
                <a className="text-title-navbar">Company</a>
                <ul>
                    <li>ABOUT</li>
                    <li>OUR WORK</li>
                    <li>OUR TEAM</li>
                    <li>CONTACT</li>
                </ul>
            </nav>
        </header>
    )
}

export default Header