import Footer from "../../layouts/Footer"
import Header from "../../layouts/Header"
import img1 from "../../assets/img/bg.jpg"
import img2 from "../../assets/img/about-bg.jpg"
import lightbulb from "../../assets/img/lightbulb-o.png"
import bank from "../../assets/img/bank.png"
import balancedScale from "../../assets/img/balance-scale.png"
import "./home.css"
import { useState } from "react"

function Home() {
   const [name, SetName] = useState("")
   const [email, SetEmail] = useState("")
   const [message, SetMessage] = useState("")
   const [statusName, setStatusName] = useState(false)
   const [statusEmail, setStatusEmail] = useState(false)
   const [statusMessage, setStatusMessage] = useState(false)
   const [activeImageNum, setCurrent] = useState(0);
   
   const listPhotoCarousel = [
     {
       src:img1,
       title:"THIS IS PLACE WHERE TECHNOLOGY & CREATIVITY FUSED INTO DIGITAL CHEMISTRY"
     },
     {
       src:img2,
       title:"WE DON'T HAVE THE BEST BUT WE HAVE THE GREATEST"
     }
   ]

   const length = listPhotoCarousel.length;

   const nextSlide = () => {
      setCurrent(activeImageNum === length - 1 ? 0 : activeImageNum + 1);
   };
   
   const prevSlide = () => {
      setCurrent(activeImageNum === 0 ? length - 1 : activeImageNum - 1);
   };

   
   if (!Array.isArray(listPhotoCarousel) || listPhotoCarousel.length <= 0) {
      return null;
   }

   const handleName = (e) => {
      SetName(e.target.value)
   }

   const handleEmail = (e) => {
      SetEmail(e.target.value)
   }
 
   const handleMessage = (e) => {
      SetMessage(e.target.value)
   }

   const handleSubmit = (event) => {
     event.preventDefault()
    
     if(name === "") {
        setStatusName(false)
     } 

     if(email === "" && email.includes(".")=== false && email.includes("@") === false) {
        setStatusEmail(false)
     }

     if(message === "") {
        setStatusMessage(false)
     }
   }

    return (
        <>
        <Header />
        
        <main>
            <div className="slideshow-container">
                {listPhotoCarousel.map((item, index) => 
                        <div
                            className={index === activeImageNum ? "currentSlide active" : "currentSlide"}
                               key={index}
                             >
                            {index === activeImageNum && <img src={item.src} className="img-slider" />}
                            {index === activeImageNum &&  <p className="caption-text">{item.title}</p>}
                        </div>
                )}
    
                <a className="prev" onClick={prevSlide}>❮</a>
                <a className="next" onClick={nextSlide}>❯</a>
    
            </div>

            <div className="list-dot">
                    <span className={activeImageNum  === 0 ? "dot active" : "dot"} onClick={prevSlide}></span> 
                    <span className={activeImageNum  === 1 ? "dot active" : "dot"} onClick={nextSlide}></span> 
            </div>    

            <div className="container">
                <h1 className="title-values text-center">OUR VALUES</h1>
                <div className="row">
                    <div className="col-4 red-values values-content">
                        <img className="icon-values" src={lightbulb} alt="lightbulb-png.png"/>
                        <h3 >INNOVATIVE</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime exercitationem dolorem deserunt, unde, eaque ipsa? </p>  
                    </div>            
                    <div className="col-4 green-values values-content">
                        <img className="icon-values" src={bank} alt="bank.png"/>
                        <h3>LOYALTY</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit similique eum itaque facere temporibus dolores. </p>  
                    </div>
                    <div className="col-4 blue-values values-content">
                        <img className="icon-values" src={balancedScale} alt="balancedScale.png"/>
                        <h3>RESPECT</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, sit? Tenetur et neque quod incidunt! </p>  
                    </div>
                                            
                </div>
            </div>

            <div className="contact-us-style">
                <h1 className="text-center">CONTACT US</h1>

                <form onSubmit={handleSubmit}>
                    <label 
                       htmlFor="Name">
                        Name
                    </label><br />
                    <input 
                       className="input"
                       name="name"
                       type="text"
                       value={name}
                       onChange={handleName} /><br />
                    { name === "" ? true : statusName === false ? <p>This field is required.</p> : true}
                    <label 
                       htmlFor="Email">
                        Email
                    </label><br />
                    <input 
                       className="input" 
                       name="email"
                       type="text"
                       value={email}
                       onChange={handleEmail}
                       /><br />
                    { statusEmail === false ? <p>Invalid email address</p> : true}
                    <label 
                       htmlFor="Message" >
                       Message</label><br />
                    <textarea 
                       className="textarea-field" 
                       name="message" 
                       type="text"
                       value={message}
                       onChange={handleMessage}/>
                     { statusMessage === false ? <p>This field is required.</p> : true}

                    <button type="submit" >SUBMIT</button>
                </form>
            </div>
        </main>

        <Footer />
        </>
    )
}

export default Home